package com.backend.sevvy.controller;

import com.backend.sevvy.database.model.*;
import com.backend.sevvy.database.service.SevvyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class MainController {

    @Autowired
    private SevvyService sevvyService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    @ResponseBody
    public void handleGet(HttpServletResponse response) {
        response.setHeader("Location", "localhost:3000/");
        response.setStatus(302);
    }

    @GetMapping("/restricted")
    public String restricted(){
        return "You need to be logged in";
    }

}

