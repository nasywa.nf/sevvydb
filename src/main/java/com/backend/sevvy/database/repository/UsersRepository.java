package com.backend.sevvy.database.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.backend.sevvy.database.model.Users;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users,String>{
    List<Users> findAll();
    Users findUserByEmail(String email);

}
