package com.backend.sevvy.database.repository;
import com.backend.sevvy.database.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import com.backend.sevvy.database.model.Pendidikan;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface PendidikanRepository extends JpaRepository<Pendidikan,String>{
    List<Pendidikan> findAll();
    List<Pendidikan> findPendidikanByUsers(Users users);
    Pendidikan findPendidikanById(Long id);

    @Transactional
    void deleteById(Long id);

}
