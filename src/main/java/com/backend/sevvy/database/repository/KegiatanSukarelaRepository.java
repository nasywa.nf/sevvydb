package com.backend.sevvy.database.repository;
import com.backend.sevvy.database.model.KegiatanSukarela;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Id;
import java.util.List;
import java.util.Optional;

@Repository
public interface KegiatanSukarelaRepository extends JpaRepository<KegiatanSukarela,String>{
    List<KegiatanSukarela> findByPerusahaan(String perusahaan);
    List<KegiatanSukarela> findAllByTahunSelesaiAndTahunMulaiOrderByTahunSelesaiDesc(int tahunSelesai, int tahunMulai);
    List<KegiatanSukarela> findAll();
    List<KegiatanSukarela> findKegiatanSukarelaByUsers(Users users);
    KegiatanSukarela findKegiatanSukarelaById(Long id);

    @Transactional
    void deleteById(Long id);
}
