package com.backend.sevvy.database.repository;
import com.backend.sevvy.database.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import com.backend.sevvy.database.model.Situs;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface SitusRepository extends JpaRepository<Situs,String>{
    List<Situs> findAll();
    List<Situs> findSitusByUsers(Users users);
    Situs findSitusById(Long id);

    @Transactional
    void deleteById(Long id);
}
