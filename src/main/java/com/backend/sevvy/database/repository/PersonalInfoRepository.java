package com.backend.sevvy.database.repository;
import com.backend.sevvy.database.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import com.backend.sevvy.database.model.PersonalInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonalInfoRepository extends JpaRepository<PersonalInfo,String>{
    List<PersonalInfo> findAll();
    List<PersonalInfo> findByUsers(Users users);

}
