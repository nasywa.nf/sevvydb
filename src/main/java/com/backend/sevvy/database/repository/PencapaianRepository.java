package com.backend.sevvy.database.repository;
import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Pengalaman;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface PencapaianRepository extends JpaRepository<Pencapaian,String> {
    List<Pencapaian> findAll();
    List<Pencapaian> findByTitle(String title);
    List<Pencapaian> findPencapaianByUsers(Users users);
    Pencapaian findPencapaianById(Long id);

    @Transactional
    void deleteById(Long id);
}
