package com.backend.sevvy.database.repository;
import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import com.backend.sevvy.database.model.Keterampilan;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
public interface KeterampilanRepository extends JpaRepository<Keterampilan,String>{
    List<Keterampilan> findAll();
    List<Keterampilan> findByNamaKeterampilan(String namaKeterampilan);
    List<Keterampilan> findKeterampilanByUsers(Users users);
    Keterampilan findKeterampilanById(Long id);

    @Transactional
    void deleteById(Long id);
}
