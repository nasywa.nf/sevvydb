package com.backend.sevvy.database.model;
import com.backend.sevvy.database.model.PersonalInfo;
import javax.persistence.Id;
/*
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;*/
import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table
public class Situs implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "label")
    private String label;
    @Column(name = "link")
    private String link;
//    @Column(name = "email")
//    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
//    private PersonalInfo personalInfo;
    private Users users;

    public Situs(){

    }
    public Situs(String label, String link){
        this.label = label;
        this.link = link;
    }

    public String getLabel() {
        return label;
    }

    public String getLink() {
        return link;
    }

    public Situs setLabel(String label) {
        this.label = label;
        return this;
    }

    public Situs setLink(String link) {
        this.link = link;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setUsers(Users users){
        this.users = users;
    }

}
