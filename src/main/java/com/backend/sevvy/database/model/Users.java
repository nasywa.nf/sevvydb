package com.backend.sevvy.database.model;
import com.backend.sevvy.database.model.Keterampilan;
import com.backend.sevvy.database.model.KegiatanSukarela;
import com.backend.sevvy.database.model.Pengalaman;
import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Pendidikan;
import com.backend.sevvy.database.model.Situs;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="users")
public class Users implements Serializable {
    @Id
    @Column(name = "email", updatable = false, nullable = false)
    private String email;
//    @Column(name = "username", updatable = false, nullable = false)
//    private String username;
    @Column(name = "password", updatable = false, nullable = false)
    private String password;

    @OneToOne(mappedBy = "users", cascade = CascadeType.ALL)
    private PersonalInfo personalInfo;

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<KegiatanSukarela> kegiatanSukarelaSet = new HashSet<>();
    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Keterampilan> keterampilanSet = new HashSet<>();
    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Pencapaian> pencapaianSet = new HashSet<>();;
    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Pendidikan> pendidikanSet = new HashSet<>();
    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Pengalaman> pengalamanSet = new HashSet<>();
    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Situs> situsSet = new HashSet<>();

    public Users(){

    }
    public Users(String email, String password){
        this.email = email;
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public Set<Situs> getSitusSet() {
        return situsSet;
    }

    public Set<Pendidikan> getPendidikanSet() {
        return pendidikanSet;
    }

    public Set<Pengalaman> getPengalamanSet() {
        return pengalamanSet;
    }

    public Set<Pencapaian> getPencapaianSet() {
        return pencapaianSet;
    }

    public Set<KegiatanSukarela> getKegiatanSukarelaSet() {
        return kegiatanSukarelaSet;
    }

    public Set<Keterampilan> getKeterampilanSet() {
        return keterampilanSet;
    }
}
