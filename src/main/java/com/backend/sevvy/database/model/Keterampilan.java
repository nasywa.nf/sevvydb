package com.backend.sevvy.database.model;
import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table
public class Keterampilan implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "namaKeterampilan",nullable = false, updatable = false)
    private String namaKeterampilan;
    @Column(name = "tingkatKeterampilan", nullable = false)
    private int tingkatKeterampilan;
//    @Column(name = "email")
//    private String email;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
//    private PersonalInfo personalInfo;
    private Users users;

    public Keterampilan(){

    }
    public Keterampilan(String namaKeterampilan, int tingkatKeterampilan){
        this.namaKeterampilan = namaKeterampilan;
        this.tingkatKeterampilan = tingkatKeterampilan;
    }

    public int getTingkatKeterampilan() {
        return tingkatKeterampilan;
    }

    public String getNamaKeterampilan() {
        return namaKeterampilan;
    }

    public void setNamaKeterampilan(String namaKeterampilan) {
        this.namaKeterampilan = namaKeterampilan;
    }

    public void setTingkatKeterampilan(int tingkatKeterampilan) {
        this.tingkatKeterampilan = tingkatKeterampilan;
    }

    public void setUsers(Users users){
        this.users = users;
    }

    public Long getId() {
        return id;
    }
}
