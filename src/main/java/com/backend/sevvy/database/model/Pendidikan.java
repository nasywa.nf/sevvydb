package com.backend.sevvy.database.model;
import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table
public class Pendidikan implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "jenjang")
    private String jenjang;
    @Column(name = "namaInstitusi")
    private String namaInstitusi;
    @Column(name = "jurusan")
    private String jurusan;
    @Column(name = "tahunMulai", nullable = false)
    private int tahunMulai;
    @Column(name = "tahunSelesai")
    private int tahunSelesai;
//    @Column(name = "email")
//    private String email;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
//    private PersonalInfo personalInfo;
    private Users users;

    public Pendidikan(){

    }
    public Pendidikan(String jenjang, String namaInstitusi, String jurusan, int tahunMulai, int tahunSelesai){
        this.jenjang = jenjang;
        this.namaInstitusi = namaInstitusi;
        this.jurusan = jurusan;
        this.tahunMulai = tahunMulai;
        this.tahunSelesai = tahunSelesai;
    }

    public String getJenjang() {
        return jenjang;
    }

    public String getNamaInstitusi() {
        return namaInstitusi;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public void setNamaInstitusi(String namaInstitusi) {
        this.namaInstitusi = namaInstitusi;
    }

    public void setUsers(Users users){
        this.users = users;
    }

    public Users getUsers() {
        return users;
    }
}
