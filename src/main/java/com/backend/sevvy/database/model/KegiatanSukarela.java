package com.backend.sevvy.database.model;
import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table
public class KegiatanSukarela implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "perusahaan",nullable = false)
    private String perusahaan;
    @Column(name = "posisi")
    private String posisi;
    @Column(name = "tahunMulai", nullable = false)
    private int tahunMulai;
    @Column(name = "tahunSelesai")
    private int tahunSelesai;
    @Column(name = "deskripsi")
    private String deskripsi;
//    @Column(name = "email")
//    private String email;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
//    private PersonalInfo personalInfo;
    private Users users;

    public KegiatanSukarela() {
    }

    public KegiatanSukarela(String posisi, String perusahaan, int tahunMulai, int tahunSelesai, String deskripsi) {
        this.posisi = posisi;
        this.perusahaan = perusahaan;
        this.tahunMulai = tahunMulai;
        this.tahunSelesai = tahunSelesai;
        this.deskripsi = deskripsi;
    }

    public String getPosisi() {
        return posisi;
    }

    public KegiatanSukarela setPosisi(String posisi) {
        this.posisi = posisi;
        return this;
    }

    public String getPerusahaan() {
        return perusahaan;
    }

    public KegiatanSukarela setPerusahaan(String perusahaan) {
        this.perusahaan = perusahaan;
        return this;
    }

    public int getTahunMulai() {
        return tahunMulai;
    }

    public KegiatanSukarela setTahunMulai(int tahunMulai) {
        this.tahunMulai = tahunMulai;
        return this;
    }

    public int getTahunSelesai() {
        return tahunSelesai;
    }

    public KegiatanSukarela setTahunSelesai(int tahunSelesai) {
        this.tahunSelesai = tahunSelesai;
        return this;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public KegiatanSukarela setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setUsers(Users users){
        this.users = users;
    }
}
