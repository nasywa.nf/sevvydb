package com.backend.sevvy.database.controller;
import com.backend.sevvy.database.model.*;
import com.backend.sevvy.database.service.SevvyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/database")
public class DatabaseController {
    @Autowired
    private final SevvyService sevvyService;

    public DatabaseController(SevvyService sevvyService){
        this.sevvyService = sevvyService;
    }

    // USERS AND ACCOUNT
    @PostMapping("/register")
    public ResponseEntity createUser(@RequestBody Users user){
        return ResponseEntity.ok(sevvyService.addUser(user));
    }

    //    All Users

    //    All Users Credentials
    @GetMapping("/users/all")
    public ResponseEntity getAllUsers(){
        return ResponseEntity.ok(sevvyService.getAllUser());
    }
    //    User Credentials
    @GetMapping("/users/creds")
    public ResponseEntity getUser(@RequestParam String email){
        return ResponseEntity.ok(sevvyService.findUserByEmail(email));
    }
    //    User All Data
//    @GetMapping("/users/all-data")
//    public ResponseEntity getUserAllData(@RequestParam String email){
//      //TODO
//    }

    //    PERSONAL INFO
    @PutMapping("/profile/personal")
    public ResponseEntity updatePersonalInfo(@RequestBody PersonalInfo personalInfo){
        return ResponseEntity.ok(sevvyService.updatePersonalInfo(personalInfo));
    }
    //    Personal Info User
    @GetMapping("/profile/personal")
    public ResponseEntity getPersonalInfo(@RequestParam String email){
        return ResponseEntity.ok(sevvyService.getPersonalInfo(email));
    }

    //    SITUS
    @PostMapping("/profile/situs")
    public ResponseEntity addSitus(@RequestParam String email, @RequestBody Situs situs ){
        return ResponseEntity.ok(sevvyService.addSitus(email, situs));
    }
    //    Situs Specific User
    @GetMapping("/profile/situs/all")
    public ResponseEntity getAllSitus(@RequestParam String email){
        return ResponseEntity.ok(sevvyService.findSitusByEmail(email));
    }
    //    Specific Situs
    @GetMapping("/profile/situs")
    public ResponseEntity getSitus(@RequestParam long id){
        return ResponseEntity.ok(sevvyService.findSitusById(id));
    }
    //    Update
    @PutMapping("/profile/situs")
    public ResponseEntity updateSitus(@RequestBody Situs situs){
        return ResponseEntity.ok(sevvyService.updateSitus(situs));
    }
    //    Delete Situs
    @DeleteMapping("/profile/situs")
    public ResponseEntity deleteSitus(@RequestParam long id){
        sevvyService.deleteSitus(id);
        return ResponseEntity.ok("situs deleted");
    }

    //    PENDIDIKAN

    @PostMapping("/profile/pendidikan")
    public ResponseEntity createPendidikan(@RequestParam String email, @RequestBody Pendidikan pendidikan){
        return ResponseEntity.ok(sevvyService.addPendidikan(email, pendidikan));
    }
    //    Pendidikan Specific User
    @GetMapping("/profile/pendidikan/all")
    public ResponseEntity getAllPendidikan(@PathVariable String email){
        return ResponseEntity.ok(sevvyService.findPendidikanByEmail(email));
    }
    //    Specific Pendidikan
    @GetMapping("/profile/pendidikan")
    public ResponseEntity getPendidikan(@RequestParam Long id){
        return ResponseEntity.ok(sevvyService.findPendidikanById(id));
    }
    //    Update
    @PutMapping("/profile/pendidikan")
    public ResponseEntity findPendidikan(@RequestBody Pendidikan pendidikan){
        return ResponseEntity.ok(sevvyService.updatePendidikan(pendidikan));
    }
    //    Delete
    @DeleteMapping("/profile/pendidikan")
    public ResponseEntity deletePendidikan(@RequestParam Long id) {
        sevvyService.deletePendidikan(id);
        return ResponseEntity.ok("pendidikan deleted");
    }

    //    PENGALAMAN

    @PostMapping("/profile/pengalaman")
    public ResponseEntity createPengalaman(@RequestParam String email, @RequestBody Pengalaman pengalaman){
        return ResponseEntity.ok(sevvyService.addPengalaman(email, pengalaman));
    }
    //    Pengalaman Specific User
    @GetMapping("/profile/pengalaman/all")
    public ResponseEntity getAllPengalaman(@PathVariable String email){
        return ResponseEntity.ok(sevvyService.findPengalamanByEmail(email));
    }
    //    Specific Pengalaman
    @GetMapping("/profile/pengalaman")
    public ResponseEntity getPengalaman(@RequestParam Long id){
        return ResponseEntity.ok(sevvyService.findPengalamanById(id));
    }
    //    Update
    @PutMapping("/profile/pengalaman")
    public ResponseEntity findPengalaman(@RequestBody Pengalaman pengalaman){
        return ResponseEntity.ok(sevvyService.updatePengalaman(pengalaman));
    }
    //    Delete
//    @DeleteMapping("/profile/pengalaman")
//    public ResponseEntity deletePengalaman(@RequestParam Long id){
//        sevvyService.deletePengalaman(id);
//        return ResponseEntity.ok("pengalaman deleted");
//    }

    @DeleteMapping("/profile/pengalaman")
    public ResponseEntity deletePengalaman(@RequestParam String email, @RequestParam Long id) throws Exception{
        return ResponseEntity.ok(sevvyService.validateAndDeletePengalaman(email, id));
    }

    //    PENCAPAIAN

    @PostMapping("/profile/pencapaian")
    public ResponseEntity createPencapaian(@RequestParam String email, @RequestBody Pencapaian pencapaian){
        return ResponseEntity.ok(sevvyService.addPencapaian(email, pencapaian));
    }
    //    Pencapaian Specific User
    @GetMapping("/profile/pencapaian/all")
    public ResponseEntity getAllPencapaian(@PathVariable String email){
        return ResponseEntity.ok(sevvyService.findPencapaianByEmail(email));
    }
    //    Specific Pencapaian
    @GetMapping("/profile/pencapaian")
    public ResponseEntity getPencapaian(@RequestParam Long id){
        return ResponseEntity.ok(sevvyService.findPencapaianById(id));
    }
    //    Update
    @PutMapping("/profile/pencapaian")
    public ResponseEntity updatePencapaian(@RequestBody Pencapaian pencapaian){
        return ResponseEntity.ok(sevvyService.updatePencapaian(pencapaian));
    }
    //    Delete
    @DeleteMapping("/profile/pencapaian")
    public ResponseEntity deletePencapaian(@RequestParam Long id){
        sevvyService.deletePencapaian(id);
        return ResponseEntity.ok("pencapaian deleted");
    }
    //    KEGIATANSUKARELA

    @PostMapping("/profile/kegiatan-sukarela")
    public ResponseEntity createKegiatanSukarela(@RequestParam String email, @RequestBody KegiatanSukarela kegiatanSukarela){
        return ResponseEntity.ok(sevvyService.addKegiatanSukarela(email, kegiatanSukarela));
    }
    //    Kegiatan Sukarela Specific User
    @GetMapping("/profile/kegiatan-sukarela/all")
    public ResponseEntity getAllKegiatanSukarela(@RequestParam String email){
        return ResponseEntity.ok(sevvyService.findKegiatanSukarelaByEmail(email));
    }
    //    Specific Kegiatan Sukarela
    @GetMapping("/profile/kegiatan-sukarela")
    public ResponseEntity getKegiatanSukarela(@RequestParam Long id){
        return ResponseEntity.ok(sevvyService.findKegiatanSukarelaById(id));
    }
    //    Update
    @PutMapping("/profile/kegiatan-sukarela")
    public ResponseEntity updateKegiatanSukarela(@RequestBody KegiatanSukarela kegiatanSukarela){
        return ResponseEntity.ok(sevvyService.updateKegiatanSukarela(kegiatanSukarela));
    }
    //    Delete
    @DeleteMapping("/profile/kegiatan-sukarela")
    public ResponseEntity deleteKegiatanSukarela(@RequestParam Long id){
        sevvyService.deleteKegiatanSukarela(id);
        return ResponseEntity.ok("kegiatan deleted");
    }

    //    KETERAMPILAN

    @PostMapping("/profile/keterampilan")
    public ResponseEntity createKeterampilan(@RequestParam String email, @RequestBody Keterampilan keterampilan){
        return ResponseEntity.ok(sevvyService.addKeterampilan(email, keterampilan));
    }
    //    Keterampilan Specific User
    @GetMapping("/profile/keterampilan/all")
    public ResponseEntity getAllKeterampilan(@PathVariable String email){
        return ResponseEntity.ok(sevvyService.findKeterampilanByEmail(email));
    }
    //    Specific Keterampilan
    @GetMapping("/profile/keterampilan")
    public ResponseEntity getKeterampilan(@RequestParam Long id){
        return ResponseEntity.ok(sevvyService.findKeterampilanById(id));
    }
    //    Update
    @PutMapping("/profile/keterampilan")
    public ResponseEntity updateKeterampilan(@RequestBody Keterampilan keterampilan){
        return ResponseEntity.ok(sevvyService.updateKeterampilan(keterampilan));
    }
    //    Delete
    @DeleteMapping("/profile/keterampilan")
    public ResponseEntity deleteKeterampilan(@RequestParam Long id){
        sevvyService.deleteKeterampilan(id);
        return ResponseEntity.ok("keterampilan deleted");
    }
}

