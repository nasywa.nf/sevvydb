package com.backend.sevvy.database.service;
import com.backend.sevvy.database.model.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface SevvyService {
    public List<PersonalInfo> getAllPersonalInfo();
    public List<KegiatanSukarela> getAllKegiatanSukarela();
    public List<Keterampilan> getAllKeterampilan();
    public List<Pencapaian> getAllPencapaian();
    public List<Pendidikan> getAllPendidikan();
    public List<Pengalaman> getAllPengalaman();
    public List<Situs> getAllSitus();

    public PersonalInfo addPersonalInfo(PersonalInfo personalInfo);
    public KegiatanSukarela addKegiatanSukarela(String email, KegiatanSukarela kegiatanSukarela);
    public Keterampilan addKeterampilan(String email, Keterampilan keterampilan);
    public Pencapaian addPencapaian(String email, Pencapaian pencapaian);
    public Pendidikan addPendidikan(String email, Pendidikan pendidikan);
    public Situs addSitus(String email, Situs situs);

    public void deleteKegiatanSukarela(Long id);
    public void deleteKeterampilan(Long id);
    public void deletePencapaian(Long id);
    public void deletePendidikan(Long id);
    public void deleteSitus(Long id);
    public void deletePengalaman(Long id);

    public PersonalInfo updatePersonalInfo(PersonalInfo personalInfo);
    public Keterampilan updateKeterampilan(Keterampilan keterampilan);
    public Pencapaian updatePencapaian(Pencapaian pencapaian);
    public Pendidikan updatePendidikan(Pendidikan pendidikan);
    public KegiatanSukarela updateKegiatanSukarela(KegiatanSukarela kegiatanSukarela);

    public Situs updateSitus(Situs situs);
    public Users addUser(Users user);
    public Users validateUser(Users user);
    public List<Users> getAllUser();

    public Users findUserByEmail(String email);



    List<PersonalInfo> getPersonalInfo(String email);
    //    List<KegiatanSukarela> findPersonalInfoByEmail(String email);
    List<KegiatanSukarela> findKegiatanSukarelaByEmail(String email);
    List<Pencapaian> findPencapaianByEmail(String email);
    List<Keterampilan> findKeterampilanByEmail(String email);
    List<Pendidikan> findPendidikanByEmail(String email);
    List<Situs> findSitusByUsers(Users users);

    List<Situs> findSitusByEmail(String email);

    public Pengalaman addPengalaman(String email, Pengalaman pengalaman);
    List<Pengalaman> findAllPengalaman();
    List<Pengalaman> findPengalamanByUsers(Users users);
    List<Pengalaman> findPengalamanByEmail(String email);
    public Pengalaman updatePengalaman(Pengalaman pengalaman);

    public Situs findSitusById(Long id);
    public Pendidikan findPendidikanById(Long id);
    public KegiatanSukarela findKegiatanSukarelaById(Long id);
    public Keterampilan findKeterampilanById(Long id);
    public Pencapaian findPencapaianById(Long id);
    public Pengalaman findPengalamanById(Long id);

    public String validateAndDeletePengalaman(String email, Long id) throws Exception;
}
