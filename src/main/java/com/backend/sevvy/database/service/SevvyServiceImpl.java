package com.backend.sevvy.database.service;
import com.backend.sevvy.database.model.*;
import com.backend.sevvy.database.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class SevvyServiceImpl implements SevvyService {
    @Autowired
    PersonalInfoRepository personalInfoRepository;
    @Autowired
    PencapaianRepository pencapaianRepository;
    @Autowired
    PendidikanRepository pendidikanRepository;
    @Autowired
    KeterampilanRepository keterampilanRepository;
    @Autowired
    KegiatanSukarelaRepository kegiatanSukarelaRepository;
    @Autowired
    PengalamanRepository pengalamanRepository;
    @Autowired
    SitusRepository situsRepository;
    @Autowired
    UsersRepository usersRepository;

    @Override
    public List<PersonalInfo> getAllPersonalInfo() {
        return personalInfoRepository.findAll();
    }

    @Override
    public List<KegiatanSukarela> getAllKegiatanSukarela() {
        return kegiatanSukarelaRepository.findAll();
    }

    @Override
    public List<Keterampilan> getAllKeterampilan() {
        return keterampilanRepository.findAll();
    }

    @Override
    public List<Pencapaian> getAllPencapaian() {
        return pencapaianRepository.findAll();
    }

    @Override
    public List<Pendidikan> getAllPendidikan() {
        return pendidikanRepository.findAll();
    }

    @Override
    public List<Pengalaman> getAllPengalaman() {
        return pengalamanRepository.findAll();
    }

    @Override
    public List<Situs> getAllSitus() {
        return situsRepository.findAll();
    }

    @Override
    public PersonalInfo addPersonalInfo(PersonalInfo personalInfo) {
        personalInfoRepository.save(personalInfo);
        return personalInfo;

    }

    @Override
    public KegiatanSukarela addKegiatanSukarela(String email, KegiatanSukarela kegiatanSukarela) {
        Users users = findUserByEmail(email);
        kegiatanSukarela.setUsers(users);
        kegiatanSukarelaRepository.save(kegiatanSukarela);
        return kegiatanSukarela;
    }

    @Override
    public Keterampilan addKeterampilan(String email, Keterampilan keterampilan) {
        Users users = findUserByEmail(email);
        keterampilan.setUsers(users);
        keterampilanRepository.save(keterampilan);
        return keterampilan;
    }

    @Override
    public Pencapaian addPencapaian(String email, Pencapaian pencapaian) {
        Users users = findUserByEmail(email);
        pencapaian.setUsers(users);
        pencapaianRepository.save(pencapaian);
        return pencapaian;
    }

    @Override
    public Pendidikan addPendidikan(String email, Pendidikan pendidikan) {
        Users users = findUserByEmail(email);
        pendidikan.setUsers(users);
        pendidikanRepository.save(pendidikan);
        return pendidikan;
    }

    @Override
    public Pengalaman addPengalaman(String email, Pengalaman pengalaman) {
        Users users = findUserByEmail(email);
        pengalaman.setUsers(users);
        pengalamanRepository.save(pengalaman);
        return pengalaman;
    }

    @Override
    public Situs addSitus(String email, Situs situs) {
        Users users = findUserByEmail(email);
        situs.setUsers(users);
        situsRepository.save(situs);
        return situs;
    }

    @Override
    public void deleteKegiatanSukarela(Long id) {
        kegiatanSukarelaRepository.deleteById(id);
    }

    @Override
    public void deleteKeterampilan(Long id) {
        keterampilanRepository.deleteById(id);

    }

    @Override
    public void deletePencapaian(Long id) {
        pencapaianRepository.deleteById(id);

    }

    @Override
    public void deletePendidikan(Long id) {
        pendidikanRepository.deleteById(id);

    }

    @Override
    public void deletePengalaman(Long id) {
        pengalamanRepository.deleteById(id);
    }

    @Override
    public void deleteSitus(Long id) {
        situsRepository.deleteById(id);

    }

    @Override
    public PersonalInfo updatePersonalInfo(PersonalInfo personalInfo) {
        personalInfoRepository.save(personalInfo);
        return personalInfo;
    }

    @Override
    public Keterampilan updateKeterampilan(Keterampilan keterampilan) {
        keterampilanRepository.save(keterampilan);
        return keterampilan;
    }

    @Override
    public Pencapaian updatePencapaian(Pencapaian pencapaian) {
        pencapaianRepository.save(pencapaian);
        return pencapaian;
    }

    @Override
    public Pendidikan updatePendidikan(Pendidikan pendidikan) {
        pendidikanRepository.save(pendidikan);
        return pendidikan;
    }

    @Override
    public Pengalaman updatePengalaman(Pengalaman pengalaman) {
        pengalamanRepository.save(pengalaman);
        return pengalaman;
    }

    @Override
    public KegiatanSukarela updateKegiatanSukarela(KegiatanSukarela kegiatanSukarela) {
        KegiatanSukarela updated = kegiatanSukarelaRepository.findKegiatanSukarelaById(kegiatanSukarela.getId())
                .setPerusahaan(kegiatanSukarela.getPerusahaan())
                .setPosisi(kegiatanSukarela.getPosisi())
                .setTahunMulai(kegiatanSukarela.getTahunMulai())
                .setTahunSelesai(kegiatanSukarela.getTahunSelesai())
                .setDeskripsi(kegiatanSukarela.getDeskripsi());
        kegiatanSukarelaRepository.save(kegiatanSukarela);
        return kegiatanSukarela;
    }

    @Override
    public Situs updateSitus(Situs situs) {
//        Situs newSitus = findSitusById(id);
//        newSitus.setLabel(situs.getLabel()).setLink(situs.getLink());
        situsRepository.save(situs);
        return situs;
    }

    @Override
    public List<PersonalInfo> getPersonalInfo(String email) {
        Users users = findUserByEmail(email);
        return personalInfoRepository.findByUsers(users);
    }

    @Override
    public List<Pengalaman> findPengalamanByUsers(Users users) {
        return pengalamanRepository.findPengalamanByUsers(users);
    }
//
//    @Override
//    public List<KegiatanSukarela> findKegiatanSukarelaByUsers(Users users) {
//        return kegiatanSukarelaRepository.findKegiatanSukarelaByUsers(users);
//    }
//
//    @Override
//    public List<Pencapaian> findPencapaianByUsers(Users users) {
//        return pencapaianRepository.findPencapaianByUsers(users);
//    }
//
//    @Override
//    public List<Keterampilan> findKeterampilanByUsers(Users users) {
//        return keterampilanRepository.findKeterampilanByUsers(users);
//    }
//
//    @Override
//    public List<Pendidikan> findPendidikanByUsers(Users users); {
//        return pendidikanRepository.findPendidikanByUsers(users);
//    }

    @Override
    public List<Pengalaman> findPengalamanByEmail(String email) {
        Users users = findUserByEmail(email);
        return pengalamanRepository.findPengalamanByUsers(users);
    }

    @Override
    public List<Situs> findSitusByEmail(String email) {
        Users users = findUserByEmail(email);
        return situsRepository.findSitusByUsers(users);
    }

    @Override
    public List<KegiatanSukarela> findKegiatanSukarelaByEmail(String email) {
        Users users = findUserByEmail(email);
        return kegiatanSukarelaRepository.findKegiatanSukarelaByUsers(users);
    }

    @Override
    public List<Pencapaian> findPencapaianByEmail(String email) {
        Users users = findUserByEmail(email);
        return pencapaianRepository.findPencapaianByUsers(users);
    }

    @Override
    public List<Keterampilan> findKeterampilanByEmail(String email) {
        Users users = findUserByEmail(email);
        return keterampilanRepository.findKeterampilanByUsers(users);
    }

    @Override
    public List<Pendidikan> findPendidikanByEmail(String email) {
        Users users = findUserByEmail(email);
        return pendidikanRepository.findPendidikanByUsers(users);
    }

    @Override
    public List<Situs> findSitusByUsers(Users users) {
        return situsRepository.findSitusByUsers(users);
    }

    @Override
    public Situs findSitusById(Long id) {
        return situsRepository.findSitusById(id);
    }

    @Override
    public List<Pengalaman> findAllPengalaman() {
        return pengalamanRepository.findAll();
    }

    /**
     * ini masih ngasal ya gatau dibuat personal info gitu atau ngga
     **/
    @Override
    public Users addUser(Users user) {
        usersRepository.save(user);

        String emailUser = user.getEmail();
        PersonalInfo defaultPI = new PersonalInfo("", "", emailUser, "", "", "", "", "", "");
//        this.addPersonalInfo(defaultPI);

        defaultPI.setUsers(user);
        personalInfoRepository.save(defaultPI);
        return user;
    }


    @Override
    public List<Users> getAllUser() {
        return usersRepository.findAll();
    }

    @Override
    public Users findUserByEmail(String email) {
        return usersRepository.findUserByEmail(email);
    }

    @Override
    public Users validateUser(Users user) {
        return user;
    }

    @Override
    public Pendidikan findPendidikanById(Long id) {
        return pendidikanRepository.findPendidikanById(id);
    }

    @Override
    public KegiatanSukarela findKegiatanSukarelaById(Long id) {
        return kegiatanSukarelaRepository.findKegiatanSukarelaById(id);
    }

    @Override
    public Keterampilan findKeterampilanById(Long id) {
        return keterampilanRepository.findKeterampilanById(id);
    }

    @Override
    public Pencapaian findPencapaianById(Long id) {
        return pencapaianRepository.findPencapaianById(id);
    }

    @Override
    public Pengalaman findPengalamanById(Long id) {
        return pengalamanRepository.findPengalamanById(id);
    }

    @Override
    public String validateAndDeletePengalaman(String email, Long id) throws Exception {
        if ((pengalamanRepository.findById(id)).isPresent()) {
            pengalamanRepository.deleteById(id);
            return "pengalaman deleted";
        } else {
            throw new Exception("id not exist");
        }
    }
}