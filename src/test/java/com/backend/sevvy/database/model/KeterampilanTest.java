package com.backend.sevvy.database.model;
import com.backend.sevvy.database.model.Keterampilan;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class KeterampilanTest {
    Keterampilan keterampilan;
    @BeforeEach
    public void setUp(){
        keterampilan = new Keterampilan("mencuci",90);

    }
    @Test
    public void testNamaGetter(){assertEquals("mencuci",keterampilan.getNamaKeterampilan());}
    @Test
    public void testTingkatGetter(){assertEquals(90,keterampilan.getTingkatKeterampilan());}

    @Test
    public void testNamaSetter(){
        keterampilan.setNamaKeterampilan("abc");
        assertEquals("abc",keterampilan.getNamaKeterampilan());
    }
    @Test
    public void testTingkatSetter(){
        keterampilan.setTingkatKeterampilan(80);
        assertEquals(80,keterampilan.getTingkatKeterampilan());
    }
}
