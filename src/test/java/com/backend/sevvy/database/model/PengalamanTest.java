package com.backend.sevvy.database.model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PengalamanTest {
    Pengalaman pengalaman;
    @BeforeEach
    public void setUp(){
        pengalaman = new Pengalaman("Direktur","Holcim",2016,2018,"test");
    }
    @Test
    public void testGetter(){
        assertEquals("Direktur",pengalaman.getPosisi());
        assertEquals("Holcim",pengalaman.getPerusahaan());
        assertEquals(2016,pengalaman.getTahunMulai());
        assertEquals(2018,pengalaman.getTahunSelesai());
        assertEquals("test",pengalaman.getDeskripsi());
    }
    @Test
    public void testSetter(){
        pengalaman.setPosisi("intern");
        pengalaman.setPerusahaan("abc");
        pengalaman.setTahunMulai(2019);
        pengalaman.setTahunSelesai(2020);
        pengalaman.setDeskripsi("test2");
        assertEquals("intern",pengalaman.getPosisi());
        assertEquals("abc",pengalaman.getPerusahaan());
        assertEquals(2019,pengalaman.getTahunMulai());
        assertEquals(2020,pengalaman.getTahunSelesai());
        assertEquals("test2",pengalaman.getDeskripsi());

    }

}
