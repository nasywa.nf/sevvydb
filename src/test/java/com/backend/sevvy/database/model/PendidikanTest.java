package com.backend.sevvy.database.model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PendidikanTest {
    Pendidikan pendidikan;
    @BeforeEach
    public void setUp(){
        pendidikan = new Pendidikan("SD", "SAB");
    }
    @Test
    public void testJenjangGetter(){
        assertEquals("SD", pendidikan.getJenjang());
    }
    @Test
    public void testNamaInstitusiGetter(){
        assertEquals("SAB", pendidikan.getNamaInstitusi());
    }
    @Test
    public void testJenjangSetter(){
        pendidikan.setJenjang("SMP");
        assertEquals("SMP", pendidikan.getJenjang());
    }
    @Test
    public void testNamaInstitusiSetter(){
        pendidikan.setNamaInstitusi("Al-Azhar");
        assertEquals("Al-Azhar", pendidikan.getNamaInstitusi());

    }
}
