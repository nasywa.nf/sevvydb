package com.backend.sevvy.database.model;
import com.backend.sevvy.database.model.Pencapaian;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PencapaianTest {
    Pencapaian pencapaian;
    @BeforeEach
    public void setUp(){
        pencapaian = new Pencapaian("juara 1","BEM UI","test");
    }
    @Test
    public void testTitleGetter(){ assertEquals("juara 1",pencapaian.getTitle());}
    @Test
    public void testInstitusiGetter(){assertEquals("BEM UI",pencapaian.getInstitusi());}
    @Test
    public void testDeskripsiGetter(){assertEquals("test",pencapaian.getDeskripsi());}
    @Test
    public void testTitleSetter(){
        pencapaian.setTitle("a");
        assertEquals("a",pencapaian.getTitle());
    }
    @Test
    public void testInstitusiSetter(){
        pencapaian.setInstitusi("b");
        assertEquals("b",pencapaian.getInstitusi());
    }
    @Test
    public void testDeskripsiSetter(){
        pencapaian.setDeskripsi("c");
        assertEquals("c",pencapaian.getDeskripsi());
    }
}
