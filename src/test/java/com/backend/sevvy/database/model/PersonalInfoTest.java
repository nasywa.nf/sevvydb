package com.backend.sevvy.database.model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PersonalInfoTest {
    PersonalInfo personalInfo;
    @BeforeEach
    public void setUp(){
        personalInfo = new PersonalInfo("nasywa","fathiyah","itsmenasywa@gmail.com","012","kebonmanis","cilacap","jateng","524");

    }
    @Test
    public void testGetter(){
        assertEquals("nasywa",personalInfo.getFirstName());
        assertEquals("fathiyah",personalInfo.getLastName());
        assertEquals("itsmenasywa@gmail.com",personalInfo.getEmail());
        assertEquals("012",personalInfo.getPhoneNumber());
        assertEquals("kebonmanis",personalInfo.getAddress());
        assertEquals("cilacap",personalInfo.getCity());
        assertEquals("jateng",personalInfo.getState());
        assertEquals("524",personalInfo.getPostalCode());
    }
    @Test
    public void testSetter(){
        personalInfo.setFirstName("nasywa1");
        personalInfo.setLastName("fathiyah1");
        personalInfo.setEmail("nasywa@gmail.com");
        personalInfo.setPhoneNumber("0123");
        personalInfo.setAddress("kebon");
        personalInfo.setCity("cilacapop");
        personalInfo.setState("jatengg");
        personalInfo.setPostalCode("5245");
        assertEquals("nasywa1",personalInfo.getFirstName());
        assertEquals("fathiyah1",personalInfo.getLastName());
        assertEquals("nasywa@gmail.com",personalInfo.getEmail());
        assertEquals("0123",personalInfo.getPhoneNumber());
        assertEquals("kebon",personalInfo.getAddress());
        assertEquals("cilacapop",personalInfo.getCity());
        assertEquals("jatengg",personalInfo.getState());
        assertEquals("5245",personalInfo.getPostalCode());
    }

}
