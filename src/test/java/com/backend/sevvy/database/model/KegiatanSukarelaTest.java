package com.backend.sevvy.database.model;
import com.backend.sevvy.database.model.KegiatanSukarela;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class KegiatanSukarelaTest {
    KegiatanSukarela ks;
    @BeforeEach
    public void setUp(){
        ks = new KegiatanSukarela("intern", "pusilkom", 2016, 2016, "test");

    }
    @Test
    public void testPosisiGetterMethod(){
        assertEquals("intern",ks.getPosisi());
    }
    @Test
    public void testPerusahaanGetterMethod(){
        assertEquals("pusilkom",ks.getPerusahaan());
    }
    @Test
    public void testTahunMulaiGetterMethod(){
        assertEquals(2016,ks.getTahunMulai());
    }
    @Test
    public void testTahunSelesaiGetterMethod(){
        assertEquals(2016,ks.getTahunSelesai());
    }
    @Test
    public void testDeskripsiGetterMethod(){
        assertEquals("test",ks.getDeskripsi());
    }

    @Test
    public void testPosisiSetterMethod(){
        ks.setPosisi("manager");
        assertEquals("manager",ks.getPosisi());
    }
    @Test
    public void testPerusahaan(){
        ks.setPerusahaan("sevvy");
        assertEquals("sevvy",ks.getPerusahaan());
    }
    @Test
    public void testTahunMulai(){
        ks.setTahunMulai(2019);
        assertEquals(2019,ks.getTahunMulai());
    }
    @Test
    public void testTahunSelesai(){
        ks.setTahunSelesai(2020);
        assertEquals(2020,ks.getTahunSelesai());
    }
}
