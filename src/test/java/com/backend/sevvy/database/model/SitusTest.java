package com.backend.sevvy.database.model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class SitusTest {
    Situs situs;
    @BeforeEach
    public void setUp(){
        situs = new Situs("linkedin","linked.in/nasywa");
    }
    @Test
    public void testGetter(){
        assertEquals("linkedin",situs.getLabel());
        assertEquals("linked.in/nasywa",situs.getLink());
    }
    @Test
    public void testSetter(){
        situs.setLabel("ig");
        situs.setLink("nasy.nf");
        assertEquals("ig", situs.getLabel());
        assertEquals("nasy.nf",situs.getLink());
    }
}
